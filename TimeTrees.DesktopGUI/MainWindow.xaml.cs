﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO; 
using TimeLines.Core;
using Newtonsoft.Json;

namespace TimeTrees.DesktopGUI
{
    
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Coords coords;
        StatusBarHelper barUpdater;
        string path = $"../../../nodes.json";
        string recspath = $"../../../recs.json";
        ToolStatus currentStatus;
        FillFormHelper fillFormHelper;
        ShapesRepo shapesRepo;
        
        public MainWindow()
        {
            InitializeComponent();
            currentStatus = new ToolStatus();
            coords = new Coords();
            barUpdater = new StatusBarHelper(CoordsLbl,StatusLbl);
            fillFormHelper = new FillFormHelper(FillPerson,NameBox,BirthDateBox,DeathCheckBox,DeathDateLbl,DeathDateBox,BtnFill,canvas);
            shapesRepo = new ShapesRepo(coords, currentStatus, barUpdater, canvas,fillFormHelper);
        }
        private void BtnRect_Click(object sender, RoutedEventArgs e)
        {
            currentStatus.SetRect();
            barUpdater.UpdateStatus(currentStatus);
        }

        private void BtnSpougeLine_Click(object sender, RoutedEventArgs e)
        {
            currentStatus.SetSpouge();
            barUpdater.UpdateStatus(currentStatus);
        }
        private void BtnParentLine_Click(object sender, RoutedEventArgs e)
        {
            currentStatus.SetParent();
            barUpdater.UpdateStatus(currentStatus);
        }
        private void BtnLoad_Click(object sender, RoutedEventArgs e)
        {
            shapesRepo.ClearRectAndPoint();
            shapesRepo.ClearPolylines();
            canvas.Children.Clear();
            shapesRepo.SetNodes(JsonInput.ReadDataNodes(recspath,shapesRepo));
            shapesRepo.DrawPolylines();
            foreach (Node node in shapesRepo.nodes) canvas.Children.Add(node.Rectangle);
        }
        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            List<NodeConverter> convertNodes = new List<NodeConverter>();
            foreach(Node node in shapesRepo.nodes) convertNodes.Add(new NodeConverter(node.id, node.name, node.birthDate, node.deathDate, node.partner, node.parents, node.Rectangle.Margin));
            FileOutput.WriteDataJson<NodeConverter>(recspath, convertNodes);
        }
        private void BtnEdit_Click(object sender, RoutedEventArgs e)
        {
            currentStatus.SetEdit();
            barUpdater.UpdateStatus(currentStatus);
        }

        private void Canvas_MouseMove(object sender, MouseEventArgs e)
        {
            coords.Update(e, canvas);
            barUpdater.UpdateCoords(coords);
            shapesRepo.FindHoveredShape();
            foreach (UIElement shape in canvas.Children) shape.Effect = null;
            shapesRepo.DropShadowEffect();
            if (currentStatus.status == ToolStatus.State.None)
            {
                if (e.LeftButton == MouseButtonState.Pressed && shapesRepo.selectedRectangle != null)
                {
                    NewDragRectangle(shapesRepo);
                }
            }
            if (currentStatus.status == ToolStatus.State.Spouge| currentStatus.status == ToolStatus.State.Parent)
            {
                if (shapesRepo.selectedPolyline !=null)
                {
                    shapesRepo.UpdateSelectedRectanglePoints();
                }
            }
            canvas.Children.Clear();
            shapesRepo.DrawPolylines();
            if (shapesRepo.selectedPolyline != null) canvas.Children.Add(shapesRepo.selectedPolyline);
            foreach (Node node in shapesRepo.nodes) canvas.Children.Add(node.Rectangle);
            shapesRepo.DrawGrids();


        }
        

        private void Canvas_MouseLeftBtnDown(object sender, MouseButtonEventArgs e)
        {
            if (currentStatus.status == ToolStatus.State.Rectangle)
            {
                Node newNode = shapesRepo.CreateNewNode();
                fillFormHelper.SetNode(newNode);
                canvas.Children.Add(newNode.Rectangle);
                shapesRepo.AddRectAndPoint(newNode.Rectangle);
                fillFormHelper.EnableFillForm();
            }
        }
        public void DragRectangle(ShapesRepo shapesRepo)
        {
            List<Polyline> polyline = new List<Polyline> { };
            foreach (Polyline _polyline in shapesRepo.polylines)
                if (_polyline.Points.Contains(shapesRepo.rectAndPoint[shapesRepo.selectedRectangle])) polyline.Add(_polyline);
            foreach (Polyline _polyline in polyline)
            {
                if (polyline != null)
                {
                    shapesRepo.RemovePolyline(_polyline);
                    _polyline.Points.Remove(shapesRepo.rectAndPoint[shapesRepo.selectedRectangle]);
                }
            }
            shapesRepo.RemoveRectAndPoint(shapesRepo.selectedRectangle);
            shapesRepo.UpdateSelectedRectangleMargin();
            shapesRepo.AddRectAndPoint(shapesRepo.selectedRectangle);
            foreach (Polyline _polyline in polyline)
            {
                if (polyline != null) _polyline.Points.Add(shapesRepo.rectAndPoint[shapesRepo.selectedRectangle]);
                shapesRepo.AddPolyline(_polyline);
            }
        }
        public void NewDragRectangle(ShapesRepo shapesRepo)
        {
            shapesRepo.RemoveRectAndPoint(shapesRepo.selectedRectangle);
            shapesRepo.UpdateSelectedRectangleMargin();
            shapesRepo.AddRectAndPoint(shapesRepo.selectedRectangle);
        }
        
        private void BtnFillNode_Click(object sender, RoutedEventArgs e)
        {
            fillFormHelper.UnenableFillForm();
            
            shapesRepo.AddNode(fillFormHelper.FillNode(shapesRepo.nodes));
            fillFormHelper.ClearFillForm();
            currentStatus.SetNone();
            barUpdater.UpdateStatus(currentStatus);
        }
    }
}
//TODO помогите
//TODO починить сохранение и загрузку
//TODO кнопка супружеской связи
//TODO кнопка родительской связи
//TODO (опционально, но маст хев) TOOLS
