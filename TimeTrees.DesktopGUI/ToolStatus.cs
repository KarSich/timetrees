﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeTrees.DesktopGUI
{
    public class ToolStatus
    {
        public State status { get; private set; }
        public ToolStatus()
        {
            status = State.None;
        }
        public enum State
        {
            None,
            Rectangle,
            Spouge,
            Parent,
            Edit
        }
        public void SetNone()
        {
            status = State.None;
        }
        public void SetRect()
        {
            status = State.Rectangle;
        }
        public void SetSpouge()
        {
            status = State.Spouge;
        }
        public void SetParent()
        {
            status = State.Parent;
        }
        public void SetEdit()
        {
            status = State.Edit;
        }
    }
}
