﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.Windows.Shapes;


namespace TimeTrees.DesktopGUI
{
    public class JsonInput
    {
        public static List<Node> ReadDataNodes(string path,ShapesRepo shapesRepo)
        {
            try
            {
                string json = File.ReadAllText(path);
                List<NodeConverter> convertNodes = JsonConvert.DeserializeObject<NodeConverter[]>(json).ToList();
                List<Node> nodes = new List<Node>();
                foreach (NodeConverter convertNode in convertNodes) nodes.Add(convertNode.ToNode(shapesRepo));
                foreach (Node node in nodes) shapesRepo.AddRectAndPoint(node.Rectangle);
                return nodes;
            }
            catch (Exception e)
            {
                return new List<Node> { };
            }
        }
    }
}
