﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;

namespace TimeTrees.DesktopGUI
{
    public class Coords
    {
        public int mouseX { get; private set; }
        public int mouseY { get; private set; }
        public void Update(MouseEventArgs e, Canvas canvas)
        {
            mouseX = (int)e.GetPosition(canvas).X;
            mouseY = (int)e.GetPosition(canvas).Y;
        }

    }
}
