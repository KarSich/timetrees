﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;

namespace TimeTrees.DesktopGUI
{
    public class StatusBarHelper
    {
        private Label CoordsLbl; 
        private Label StatusLbl;
        public StatusBarHelper(Label coordsLbl, Label statusLbl)
        {
            CoordsLbl = coordsLbl;
            StatusLbl = statusLbl;
        }
        public void Update(Coords coords, ToolStatus status)
        {
            CoordsLbl.Content = $"X = {coords.mouseX}  Y = {coords.mouseY}";
            StatusLbl.Content = status.status;
        }
        public void UpdateCoords( Coords coords)
        {
            CoordsLbl.Content = $"X = {coords.mouseX}  Y = {coords.mouseY}";
        }
        public void UpdateStatus(ToolStatus status)
        {
            StatusLbl.Content = status.status;
        }
    }
}
