﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows;
using System.Windows.Media;
using System.Windows.Input;

namespace TimeTrees.DesktopGUI
{
    public class FillFormHelper
    {
        private GroupBox FillPerson;
        private TextBox NameBox;
        private DatePicker BirthDateBox;
        private CheckBox DeathCheckBox;
        private Label DeathDateLbl;
        private DatePicker DeathDateBox;
        private Button BtnFill;
        private Canvas Canvas;
        private Node node;
        public FillFormHelper(   GroupBox FillPerson,
                                 TextBox NameBox,
                                 DatePicker BirthDateBox,
                                 CheckBox DeathCheckBox,
                                 Label DeathDateLbl,
                                 DatePicker DeathDateBox,
                                 Button BtnFill,
                                 Canvas Canvas )
        {
            this.FillPerson = FillPerson;
            this.FillPerson.MouseMove += FillPerson_MouseMove;
            this.NameBox = NameBox;
            this.BirthDateBox = BirthDateBox;
            this.DeathCheckBox = DeathCheckBox;
            this.DeathCheckBox.Checked += DeathCheckBox_Checked;
            this.DeathCheckBox.Unchecked += DeathCheckBox_Unchecked;
            this.DeathDateLbl = DeathDateLbl;
            this.DeathDateBox = DeathDateBox;
            this.BtnFill = BtnFill;
            this.Canvas = Canvas;
        }
        public void ClearFillForm()
        {
            node = null;
            NameBox.Text = null;
            DeathDateBox.SelectedDate = null;
            BirthDateBox.SelectedDate = null;
            DeathCheckBox.IsChecked = false;
        }
        public void SetNode(Node node)
        {
            this.node = node;
            if (this.node.birthDate != default) FillForm();
        }
        private void FillForm()
        {
            NameBox.Text = node.name;
            BirthDateBox.SelectedDate = node.birthDate;
            if (node.deathDate != default)
            {
                DeathDateBox.SelectedDate = node.deathDate;
                DeathCheckBox.IsChecked = true;
            }
        }
        private void FillPerson_MouseMove(object sender, MouseEventArgs e)
        {
            BtnFill.IsEnabled = true;
            if (BirthDateBox.SelectedDate == null) BtnFill.IsEnabled = false;
            if ((DeathDateBox.SelectedDate == null) & (DeathCheckBox.IsChecked.Value)) BtnFill.IsEnabled = false;
            if (BirthDateBox.SelectedDate > DateTime.Today) BtnFill.IsEnabled = false;
            if (DeathDateBox.SelectedDate > DateTime.Today) BtnFill.IsEnabled = false;
            if (BirthDateBox.SelectedDate > DeathDateBox.SelectedDate) BtnFill.IsEnabled = false;
        }
        public void EnableFillForm()
        {
            FillPerson.Visibility = Visibility.Visible;
            Canvas.IsEnabled = false;
        }
        public void UnenableFillForm()
        {
            FillPerson.Visibility = Visibility.Hidden;
            Canvas.IsEnabled = true;
        }
        public Node FillNode(List<Node> nodes)
        {
            if (node.birthDate==default) node.id = nodes.Count + 1;
            node.name = NameBox.Text;
            node.birthDate = BirthDateBox.SelectedDate.Value;
            if (DeathCheckBox.IsChecked.Value)
            {
                node.deathDate = DeathDateBox.SelectedDate.Value;
                node.Rectangle.Fill = Brushes.Red;
            }
            else node.deathDate = null;
            return node;
        }
        private void DeathCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            DeathDateLbl.IsEnabled = true;
            DeathDateBox.IsEnabled = true;
        }

        private void DeathCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            DeathDateLbl.IsEnabled = false;
            DeathDateBox.IsEnabled = false;
        }
    }
}
