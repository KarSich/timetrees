﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Shapes;
using System.Windows.Media;
using TimeLines.Core;

namespace TimeTrees.DesktopGUI
{
    public class NodeConverter : Person
    {
        public int partner { get; set; }
        public new List<int> parents { get; set; }
        public Thickness margin { get; set; }

        public NodeConverter (int id, string name, DateTime birthDate, DateTime? deathDate,int partner,List<int> parents, Thickness margin)
        {
            this.id = id;
            this.name = name;
            this.birthDate = birthDate;
            this.deathDate = deathDate;
            this.partner = partner;
            this.parents = parents;
            this.margin = margin;
        }
        public Node ToNode(ShapesRepo shapesRepo)
        {
            Node node = new Node
            {
                id = id,
                birthDate = birthDate,
                deathDate = deathDate,
                name = name,
                parents = parents,
                partner = partner,
                Rectangle = new Rectangle
                { Height = 101, Width = 101, Margin = margin, Stroke = Brushes.Black, StrokeThickness = 2}
            };
            node.Rectangle.MouseLeftButtonDown += shapesRepo.Rect_MouseLeftButtonDown;
            node.Rectangle.MouseLeftButtonUp += shapesRepo.Rect_MouseLeftBtnUp;
            if (node.deathDate == null) node.Rectangle.Fill = Brushes.Green;
            else node.Rectangle.Fill = Brushes.Red;
            return node;
        }
        
    }
}
