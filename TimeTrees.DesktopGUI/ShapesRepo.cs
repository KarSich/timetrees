﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.Windows.Shapes;

namespace TimeTrees.DesktopGUI
{
    public class ShapesRepo
    {
        public const int RECT_HEIGHT = 101;
        public const int RECT_WIDTH = 101;
        private Coords coords;
        private ToolStatus currentStatus;
        private StatusBarHelper barUpdater;
        private Canvas Canvas;
        private FillFormHelper fillFormHelper;
        public Shape hoveredShape { get; private set; }
        public List<Polyline> polylines { get; private set; }
        public Polyline selectedPolyline { get; private set; }
        public Rectangle selectedRectangle { get; private set; }
        public List<Node> nodes { get; private set; }
        public Dictionary<Rectangle, Point> rectAndPoint { get; private set; }
        public ShapesRepo(Coords coords, ToolStatus currentStatus, StatusBarHelper barUpdater, Canvas Canvas, FillFormHelper fillFormHelper)
        {
            this.coords = coords;
            this.currentStatus = currentStatus;
            this.barUpdater = barUpdater;
            this.fillFormHelper = fillFormHelper;
            this.Canvas = Canvas;
            Init();
        }
        private void Init()
        {
            polylines = new List<Polyline>();
            nodes = new List<Node>();
            rectAndPoint = new Dictionary<Rectangle, Point>();
        }

        public void FindHoveredShape()
        {
            hoveredShape = null;
            Shape exampleShape = new Rectangle();
            TextBox exampleBox = new TextBox();
            for(int canvasChildren = 0;canvasChildren< Canvas.Children.Count; canvasChildren++)
            {
                if (Canvas.Children[canvasChildren].IsMouseOver & ReferenceEquals(Canvas.Children[canvasChildren].GetType(), exampleShape.GetType())) hoveredShape = (Shape)Canvas.Children[canvasChildren];
            }
            //foreach (Shape shape in Canvas.Children) if (shape.IsMouseOver) hoveredShape = shape;
        }
        public void DropShadowEffect()
        {
            if (hoveredShape != null) hoveredShape.Effect = new DropShadowEffect
            {
                Color = Colors.Red,
                ShadowDepth = 0,
                Direction = 0,
                BlurRadius = 25
            };
        }
        public void DrawGrids()
        {
            foreach (Node node in nodes)
            {
                Grid nodeGrid = new Grid { Margin = node.Rectangle.Margin, Height = node.Rectangle.Height, Width = node.Rectangle.Width, IsHitTestVisible = false };
                nodeGrid.Visibility = Visibility.Visible;
                for (int rowCount = 0; rowCount < 4; rowCount++) nodeGrid.RowDefinitions.Add(new RowDefinition());
                TextBlock txtName = new TextBlock { Text = $" {node.name}", FontSize = 10, FontWeight = FontWeights.Bold};
                Grid.SetRow(txtName, 0);
                TextBlock txtBirthDate = new TextBlock { Text = $" {node.birthDate.ToShortDateString()}", FontSize = 10, FontWeight = FontWeights.Bold };
                Grid.SetRow(txtBirthDate, 1);
                TextBlock txtDeathDate = new TextBlock { FontSize = 10, FontWeight = FontWeights.Bold };
                Grid.SetRow(txtDeathDate, 2);
                TextBlock txtAge = new TextBlock { FontSize = 10, FontWeight = FontWeights.Bold };
                Grid.SetRow(txtAge, 3);
                if (node.deathDate != null)
                {
                    txtDeathDate.Text = $" {node.deathDate.Value.ToShortDateString()}";
                    txtAge.Text = $" Возраст: {(node.deathDate.Value-node.birthDate).Days/365}";
                }
                else txtAge.Text = $" Возраст: {(DateTime.Now - node.birthDate).Days / 365}";
                nodeGrid.Children.Add(txtName);
                nodeGrid.Children.Add(txtBirthDate);
                nodeGrid.Children.Add(txtDeathDate);
                nodeGrid.Children.Add(txtAge);
                Canvas.Children.Add(nodeGrid);
            }
        }
        public Node FoundPeopleById(int id)
        {
            foreach (Node node in nodes) if (node.id == id) return node;
            return null;
        }
        public void UpdateSelectedRectangleMargin()
        {
            selectedRectangle.Margin = new Thickness
            {
                Left = coords.mouseX - selectedRectangle.Width / 2,
                Top = coords.mouseY - selectedRectangle.Height / 2
            };
        }
        public void UpdateSelectedRectanglePoints()
        {
            selectedPolyline.Points[selectedPolyline.Points.Count - 1] =
                        new Point
                        {
                            X = coords.mouseX,
                            Y = coords.mouseY
                        };
        }
        public Rectangle CreateNewRectangle()
        {
            Rectangle newRect = new Rectangle
            {
                Height = RECT_HEIGHT,
                Width = RECT_WIDTH,
                Fill = Brushes.Green,
                Stroke = Brushes.Black,
                StrokeThickness = 2,
            };
            newRect.Margin = new Thickness()
            {
                Left = coords.mouseX - newRect.Width / 2,
                Top = coords.mouseY - newRect.Height / 2
            };
            newRect.MouseLeftButtonDown += Rect_MouseLeftButtonDown;
            newRect.MouseLeftButtonUp += Rect_MouseLeftBtnUp;
            return newRect;
        }
        public void Rect_MouseLeftBtnUp(object sender, MouseEventArgs e)
        {
            selectedRectangle = null;
        }
        public void Rect_MouseLeftButtonDown(object sender, MouseEventArgs e)
        {
            //RemoveGrids();
            if (currentStatus.status == ToolStatus.State.None)
            {
                selectedRectangle = (Rectangle)hoveredShape;
            }
            if (currentStatus.status == ToolStatus.State.Spouge | currentStatus.status == ToolStatus.State.Parent)
            {
                if (selectedPolyline == null)
                {
                    selectedPolyline = CreateNewPolyline();
                    polylines.Add(selectedPolyline);
                }
                else
                {
                    Point point = rectAndPoint[(Rectangle)hoveredShape];
                    if (currentStatus.status == ToolStatus.State.Spouge) point.X -= hoveredShape.Width / 2;
                    else point.Y += hoveredShape.Height / 2;
                    selectedPolyline.Points[selectedPolyline.Points.Count - 1] = point;
                    point = selectedPolyline.Points[0];
                    if (currentStatus.status == ToolStatus.State.Spouge) point.X -= hoveredShape.Width / 2;
                    else point.Y += hoveredShape.Height / 2;
                    Rectangle firstRect = rectAndPoint.Where(x => x.Value == point).FirstOrDefault().Key;
                    Node firstNode = null;
                    Node secondNode = null;
                    foreach (Node node in nodes)
                    {
                        if (node.Rectangle == firstRect)
                            firstNode = node;
                        if (node.Rectangle == (Rectangle)hoveredShape)
                            secondNode = node;
                    }
                    if(firstNode!=null&&secondNode!=null)
                    {
                        if (currentStatus.status == ToolStatus.State.Spouge)
                        {
                            firstNode.partner = secondNode.id;
                            secondNode.partner = firstNode.id;

                        }
                        else
                        {
                            if (firstNode.birthDate<secondNode.birthDate)
                            {
                                Point parentPoint = selectedPolyline.Points[0];
                                parentPoint.Y += hoveredShape.Height;
                                selectedPolyline.Points[0] = parentPoint;
                                Point childPoint = selectedPolyline.Points[1];
                                childPoint.Y -= hoveredShape.Height;
                                selectedPolyline.Points[1] = childPoint;
                                secondNode.parents.Add(firstNode.id);
                                //if (firstNode.partner != default) secondNode.parents.Add(firstNode.partner);
                            }
                            else
                            {
                                firstNode.parents.Add(secondNode.id);
                            }

                        }
                    }
                    DrawPolylines();
                    selectedPolyline = null;
                    currentStatus.SetNone();
                    barUpdater.UpdateStatus(currentStatus);
                }
            }
            if (currentStatus.status == ToolStatus.State.Edit)
            {
                fillFormHelper.EnableFillForm();
                Node editableNode = null;
                foreach (Node node in nodes) if ((Rectangle)hoveredShape == node.Rectangle) editableNode = node;
                if (editableNode != null)
                { 
                    fillFormHelper.SetNode(editableNode);
                    nodes.Remove(editableNode);
                }
            }
        }
        public void DrawPolylines()
        {
            foreach (Node node in nodes)
            {
                if (node.partner != default)
                {
                    Node partnerNode = FoundPeopleById(node.partner);
                    if (partnerNode != null)
                    {
                        Polyline partnerPolyline = new Polyline
                        {
                            Stroke = Brushes.Black,
                            StrokeThickness = 3
                        };
                        Point nodePoint = rectAndPoint[node.Rectangle];
                        Point partnerPoint = rectAndPoint[partnerNode.Rectangle];
                        if (nodePoint.X < partnerPoint.X)
                        {
                            nodePoint.X += node.Rectangle.Width / 2;
                            partnerPoint.X -= partnerNode.Rectangle.Width / 2;
                        }
                        else
                        {
                            nodePoint.X -= node.Rectangle.Width / 2;
                            partnerPoint.X += partnerNode.Rectangle.Width / 2;
                        }
                        partnerPolyline.Points.Add(nodePoint);
                        partnerPolyline.Points.Add(partnerPoint);
                        Canvas.Children.Add(partnerPolyline);
                    }
                }
                if (node.parents.Count != 0)
                {
                    foreach (int parentId in node.parents)
                    {
                        Node parentNode = FoundPeopleById(parentId);
                        if (parentNode != null)
                        {
                            Polyline parentPolyline = new Polyline
                            {
                                Stroke = Brushes.Black,
                                StrokeThickness = 3
                            };
                            Point nodePoint = rectAndPoint[node.Rectangle];
                            Point parentPoint = rectAndPoint[parentNode.Rectangle];
                            nodePoint.Y -= node.Rectangle.Height / 2;
                            parentPoint.Y += parentNode.Rectangle.Height / 2;
                            parentPolyline.Points.Add(nodePoint);
                            parentPolyline.Points.Add(parentPoint);
                            Canvas.Children.Add(parentPolyline);
                        }
                    }
                }
            }
        }
        public Polyline CreateNewPolyline()
        {
            Polyline newPolyline = new Polyline
            {
                Stroke = Brushes.Black,
                StrokeThickness = 3
            };
            Point point = rectAndPoint[(Rectangle)hoveredShape];
            if (currentStatus.status==ToolStatus.State.Spouge)
                point.X += hoveredShape.Width / 2;
            else if (currentStatus.status == ToolStatus.State.Parent)
                point.Y -= hoveredShape.Height / 2;
            newPolyline.Points.Add(point);
            newPolyline.Points.Add(rectAndPoint[(Rectangle)hoveredShape]);
            return newPolyline;
        }
        public void RemovePolyline(Polyline polyline)
        {
            polylines.Remove(polyline);
        }
        public void AddPolyline(Polyline polyline)
        {
            polylines.Add(polyline);
        }
        public void ClearPolylines()
        {
            polylines.Clear();
        }
        public void SetNodes(List<Node> nodes)
        {
            this.nodes = nodes;
        }
        public void AddNode(Node node)
        {
            nodes.Add(node);
        }
        public void AddRectAndPoint(Rectangle rectangle)
        {
            rectAndPoint.Add(rectangle, new Point { X = rectangle.Margin.Left+rectangle.Width/2, Y = rectangle.Margin.Top+rectangle.Height/2 });
        }
        public void RemoveRectAndPoint(Rectangle rectangle)
        {
            rectAndPoint.Remove(rectangle);
        }
        public void ClearRectAndPoint()
        {
            rectAndPoint.Clear();
        }
        public Node CreateNewNode()
        {
            return new Node { Rectangle = CreateNewRectangle(), parents = new List<int>() };
        }

    }
}
