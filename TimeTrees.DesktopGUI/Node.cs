﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Shapes;
using TimeLines.Core;

namespace TimeTrees.DesktopGUI
{
    public class Node : Person
    {
            public int partner { get; set; }
            public new List<int> parents { get; set; }
            public Rectangle Rectangle { get; set; }
    }
}
