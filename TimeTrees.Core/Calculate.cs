﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeLines.Core
{
    public class Calculate
    {
        public static (DateTime, DateTime) CalculatingDifference(DateTime year, DateTime minDate, DateTime maxDate)
        {
            if (year > maxDate) maxDate = year;
            if (year < minDate) minDate = year;
            return (minDate, maxDate);
        }

        public static int[] DifferenceMinAndMax(DateTime minDate, DateTime maxDate)
        {
            int[] timeVariants = new int[3];
            TimeSpan diff = maxDate - minDate;
            timeVariants[0] = (int)diff.TotalDays / 365;
            timeVariants[1] = (int)diff.TotalDays / 12;
            timeVariants[2] = (int)diff.TotalDays;
            return timeVariants;
        }
        public static int NameSpaceCalculator(List<Person> people)
        {
            int maxLenghtName = 0;
            foreach (Person person in people)
                if (person.name.Length > maxLenghtName)
                    maxLenghtName = person.name.Length;
            return maxLenghtName;
        }
        public static int IdSpaceCalculator(List<Person> people)
        {
            int maxLenghtId = 0;
            foreach (Person person in people)
                if (person.id.ToString().Length > maxLenghtId)
                    maxLenghtId = person.id.ToString().Length;
            return maxLenghtId;
        }
        public static List<Person> FindingPeople(List<Person> people, string name)
        {
            List<Person> foundPeople = new List<Person>();
            foreach (Person person in people)
                if (person.name.Contains(name))
                    foundPeople.Add(person);
            return foundPeople;
        }
        public static string NameBuilder(List<char> nameChars)
        {
            StringBuilder name = new StringBuilder("");
            if (nameChars.Count > 0)
                foreach (Char _char in nameChars)
                    name.Append(_char);
            return name.ToString();
        }
    }
}
