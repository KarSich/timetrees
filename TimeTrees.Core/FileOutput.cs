﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;

namespace TimeLines.Core
{
    public class FileOutput
    {
        public static void WriteDataJson<T>(string path, List<T> list)
        {
            JsonSerializer serializer = new JsonSerializer();
            using StreamWriter sw = new StreamWriter(path);
            using JsonTextWriter writer = new JsonTextWriter(sw);
            serializer.Serialize(writer, list);
        }
    }
}
