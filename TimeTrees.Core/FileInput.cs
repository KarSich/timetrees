﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;

namespace TimeLines.Core
{
    public class FileInput
    {
        public static Person[] ReadDataPeopleJson(string path)
        {
            string json = File.ReadAllText(path);
            return JsonConvert.DeserializeObject<Person[]>(json);
        }
        public static TimelineEvent[] ReadDataTimeLineJson(string path)
        {
            string json = File.ReadAllText(path);
            return JsonConvert.DeserializeObject<TimelineEvent[]>(json);
        }
    }
}
