﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeLines.Core
{
    public class Person
    {
        public int id { get; set; }
        public string name { get; set; }
        public DateTime birthDate { get; set; }
        public DateTime? deathDate { get; set; }
        public List<Person> parents { get; set; }

    }
}
