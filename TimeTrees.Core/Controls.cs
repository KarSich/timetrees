﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeLines.Core
{
    public class Controls
    {
        public static int DownSelect(int id, int maxId)
        {
            return id == maxId ? 1 : ++id;
        }
        public static int UpSelect(int id, int maxId)
        {
            return id == 1 ? maxId : --id;
        }
    }
}