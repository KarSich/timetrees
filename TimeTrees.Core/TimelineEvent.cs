﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeLines.Core
{
    public class TimelineEvent
    {
        public DateTime year { get; set; }
        public string description { get; set; }
        public List<Person> participants { get; set; }
    }
}
