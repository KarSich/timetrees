﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeLines.Core;

namespace TimeLines.ConsoleGUI
{
    class ObjectCreator
    {
        public static void TimeLine(List<TimelineEvent> timeLines, List<Person> people)
        {
            TimelineEvent newTimeLine = new TimelineEvent();
            newTimeLine.year = DataInput.EnterYear();
            newTimeLine.description = DataInput.EnterDescription();
            newTimeLine.participants = DataInput.EnterParticipants(people);
            timeLines.Add(newTimeLine);
        }
        public static void Person(List<Person> people)
        {
            Person newPerson = new Person { };
            newPerson.id = people.Count + 1;
            newPerson.name = DataInput.EnterName();
            newPerson.birthDate = DataInput.EnterBirthDate();
            newPerson.deathDate = DataInput.EnterDeathDate();
            newPerson.parents = DataInput.EnterParents(people);
            people.Add(newPerson);
        }
    }
}