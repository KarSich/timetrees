﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeLines.Core;

namespace TimeLines.ConsoleGUI
{
    class MainMenuItem
    {
        public static void MaxDateMinusMinDate(List<TimelineEvent> timeLines)
        {
            DateTime minDate = DateTime.MaxValue;
            DateTime maxDate = DateTime.MinValue;
            foreach (TimelineEvent _event in timeLines)
            {
                (minDate, maxDate) = Calculate.CalculatingDifference(_event.year, minDate, maxDate);
            }
            DataOutput.PrintDiffBetweenMinDateAndMaxDate(Calculate.DifferenceMinAndMax(minDate, maxDate));
        }
        public static void PeoplesLeapAndLess20(List<Person> people)
        {
            foreach (Person person in people)
            {
                int lifeTime = person.deathDate == null
                    ? DateTime.Now.Year - person.birthDate.Year
                    : person.birthDate.Year - person.birthDate.Year;
                if ((DateTime.IsLeapYear(person.birthDate.Year) == true) & (lifeTime <= 20))
                    DataOutput.PrintName(person.name);
            }
        }
        public static Person SearchPeople(List<Person> people)
        {
            ConsoleHelper.ClearScreen();
            int id = 1;
            int foundPeopleLenght = 0;
            bool isMenuActive = true;
            List<char> nameChars = new List<char> { };
            List<Person> foundPeople = new List<Person> { };
            while (isMenuActive)
            {
                Console.WriteLine("Введите имя");
                string name = Calculate.NameBuilder(nameChars);
                Console.WriteLine(name);
                foundPeople = Calculate.FindingPeople(people, name);
                if (foundPeople.Count != foundPeopleLenght)
                    id = 1;
                foundPeopleLenght = foundPeople.Count;
                DataOutput.PrintFoundPeople(foundPeople, id);
                ConsoleKeyInfo key = Console.ReadKey(true);
                ConsoleHelper.ClearScreen();
                if (key.Key == ConsoleKey.DownArrow)
                {
                    id = Controls.DownSelect(id, foundPeople.Count);
                }
                else if (key.Key == ConsoleKey.UpArrow)
                {
                    id = Controls.UpSelect(id, foundPeople.Count);
                }
                else if (key.Key == ConsoleKey.Enter)
                {
                    id--;
                    isMenuActive = false;
                }
                else if (key.Key == ConsoleKey.Backspace)
                {
                    if (nameChars.Count != 0)
                        nameChars.RemoveAt(nameChars.Count - 1);
                }
                else nameChars.Add(key.KeyChar);
            }
            DataOutput.PrintSelectedPersonName(foundPeople[id].name);
            return foundPeople[id];
        }
        public static void EditPeople(List<Person> people)
        {
            Person modifiedPerson = SearchPeople(people);
            ConsoleHelper.WatchingResult();
            bool isMenuActive = true;
            while (isMenuActive)
            {
                ConsoleHelper.ClearScreen();
                DataOutput.PrintDataModifiedPerson(modifiedPerson);
                ConsoleKeyInfo key = Console.ReadKey(true);
                ConsoleHelper.ClearScreen();
                if (key.Key == ConsoleKey.D1)
                    modifiedPerson.name = DataInput.EnterName();
                else if (key.Key == ConsoleKey.D2)
                    modifiedPerson.birthDate = DataInput.EnterBirthDate();
                else if (key.Key == ConsoleKey.D3)
                    modifiedPerson.deathDate = DataInput.EnterDeathDate();
                else if (key.Key == ConsoleKey.D4)
                    modifiedPerson.parents = DataInput.EnterParents(people);
                else if (key.Key == ConsoleKey.Escape)
                {
                    if (modifiedPerson.deathDate < modifiedPerson.birthDate)
                    {
                        isMenuActive = true;
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("Ошибка : Человек не может родиться после своей смерти");
                        Console.ForegroundColor = ConsoleColor.White;
                        ConsoleHelper.WatchingResult();
                    }
                    else isMenuActive = false;
                }
            }
            people[modifiedPerson.id-1] = modifiedPerson;
        }
    }
}
