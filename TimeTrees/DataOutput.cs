﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeLines.Core;

namespace TimeLines.ConsoleGUI
{
    class DataOutput
    {
        public static void PrintFoundPeople(List<Person> foundPeople, int id)
        {
            int maxLenghtName = Calculate.NameSpaceCalculator(foundPeople);
            int maxLenghtId = Calculate.IdSpaceCalculator(foundPeople);
            for (int i = 0; i < foundPeople.Count; i++)
            {
                if (i == id-1)
                    Console.ForegroundColor = ConsoleColor.DarkYellow;
                if (foundPeople[i].deathDate == null)
                    Console.WriteLine($"{foundPeople[i].id}{new string(' ', Math.Abs(maxLenghtId - foundPeople[i].id.ToString().Length))} {foundPeople[i].name}{new string(' ', Math.Abs(maxLenghtName - foundPeople[i].name.Length))} {foundPeople[i].birthDate} жив");
                else
                    Console.WriteLine($"{foundPeople[i].id}{new string(' ', Math.Abs(maxLenghtId - foundPeople[i].id.ToString().Length))} {foundPeople[i].name}{new string(' ', Math.Abs(maxLenghtName - foundPeople[i].name.Length))} {foundPeople[i].birthDate} {foundPeople[i].deathDate}");
                Console.ForegroundColor = ConsoleColor.White;
            }
        }
        public static void PrintDataModifiedPerson(Person modifiedPerson)
        {
            Console.WriteLine("Выберите изменяемую характеристику, напечатав ее номер. ");
            Console.WriteLine("Для выхода из меню изменения нажмите Esc");
            Console.WriteLine($"1. Имя - {modifiedPerson.name}");
            Console.WriteLine($"2. Дата Рождения - {modifiedPerson.birthDate}");
            if (modifiedPerson.deathDate == null)
                Console.WriteLine($"3. Дата Смерти - отсутствует");
            else Console.WriteLine($"3. Дата Смерти - {modifiedPerson.deathDate}");
            if (modifiedPerson.parents.Count != 0)
            {
                if (modifiedPerson.parents.Count == 1)
                    Console.WriteLine($"4. Родители - {modifiedPerson.parents[0].name}");
                else Console.WriteLine($"4. Родители - {modifiedPerson.parents[0].name} , {modifiedPerson.parents[1].name}");
            }
            else Console.WriteLine($"4. Родители - отсутствуют");
        }
        public static void PrintName(string name)
        {
            Console.WriteLine(name);
        }
        public static void PrintDiffBetweenMinDateAndMaxDate(int[] timeVariants)
        {
            Console.WriteLine($"Максимальная разница между событиями - {timeVariants[0]} лет, {timeVariants[1]} месяцев и {timeVariants[2]} дней");
        }
        public static void PrintSelectedPersonName(string name)
        {
            Console.WriteLine($"Выбранный человек - {name}");
        }
    }
}
