﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeLines.ConsoleGUI
{
    class ConsoleHelper
    {
        public static void ClearScreen()
        {
            for (int y = 0; y < Console.WindowHeight; y++)
            {
                Console.SetCursorPosition(0, y);
                Console.Write(new string(' ', Console.WindowWidth));
            }
            Console.SetCursorPosition(0, 0);
        }
        public static void WatchingResult()
        {
            Console.WriteLine("Enter any key to continie . . .");
            Console.ReadKey(true);
            ClearScreen();
        }
    }
}
