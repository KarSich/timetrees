﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Globalization;
using System.Text;
using TimeLines.Core;
namespace TimeLines.ConsoleGUI
{

    class MainMenu
    {
        static void Main(string[] args)
        {
            Console.CursorVisible = false;
            string timelinePath = @"..\..\..\timeline.json";
            string peoplePath = @"..\..\..\people.json";
            List<TimelineEvent> timeLines = new(FileInput.ReadDataTimeLineJson(timelinePath));
            List<Person> peoples = new(FileInput.ReadDataPeopleJson(peoplePath));
            List<string> Menu = new List<string>() {
                "1. Разница между самым ранним и самым поздним событием",
                "2. Имена людей, родившихся в високосный год и возрастом менее 20 лет",
                "3. Ввод данных для события и человека",
                "4. Поиск людей",
                "5. Редактирование данных о человеке",
                "6. Выход"
                };
            bool isMenuActive = true;
            int id = 1;
            while (isMenuActive)
            {
                for (
                    int i = 0; i < Menu.Count; i++)
                {
                    if (i == id - 1)
                        Console.ForegroundColor = ConsoleColor.DarkYellow;
                    Console.WriteLine(Menu[i]);
                    Console.ForegroundColor = ConsoleColor.White;
                }
                ConsoleKeyInfo key = Console.ReadKey(true);
                ConsoleHelper.ClearScreen();
                if (key.Key == ConsoleKey.DownArrow)
                {
                    id = Controls.DownSelect(id, Menu.Count);
                }
                if (key.Key == ConsoleKey.UpArrow)
                {
                    id = Controls.UpSelect(id, Menu.Count);
                }
                if (key.Key == ConsoleKey.Enter)
                {
                    switch (id)
                    {
                        case 1:
                            MainMenuItem.MaxDateMinusMinDate(timeLines);
                            ConsoleHelper.WatchingResult();
                            break;
                        case 2:
                            MainMenuItem.PeoplesLeapAndLess20(peoples);
                            ConsoleHelper.WatchingResult();
                            break;
                        case 3:
                            EnterDataMenu.EnterData(timeLines, peoples);
                            ConsoleHelper.ClearScreen();
                            break;
                        case 4:
                            MainMenuItem.SearchPeople(peoples);
                            ConsoleHelper.WatchingResult();
                            break;
                        case 5:
                            MainMenuItem.EditPeople(peoples);
                            ConsoleHelper.ClearScreen();
                            break;
                        case 6:
                            Console.WriteLine("Выход из программы...");
                            isMenuActive = false;
                            break;
                    }
                }
            }
            FileOutput.WriteDataJson(peoplePath, peoples);
            FileOutput.WriteDataJson(timelinePath, timeLines);
        }
    }
}