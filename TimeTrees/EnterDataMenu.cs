﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeLines.Core;

namespace TimeLines.ConsoleGUI
{
    class EnterDataMenu
    {
        public static void EnterData(List<TimelineEvent> timeLines, List<Person> people)
        {
            List<string> MiniMenu = new List<string>() {
                "1. Ввод события",
                "2. Ввод человека ",
                "3. Выход в главное меню",
                };
            bool isMiniMenuActive = true;
            int id = 1;
            while (isMiniMenuActive)
            {
                for (int i = 0; i < MiniMenu.Count; i++)
                {
                    if (i == id - 1)
                        Console.ForegroundColor = ConsoleColor.DarkYellow;
                    Console.WriteLine(MiniMenu[i]);
                    Console.ForegroundColor = ConsoleColor.White;
                }
                ConsoleKeyInfo key = Console.ReadKey(true);
                ConsoleHelper.ClearScreen();
                if (key.Key == ConsoleKey.DownArrow)
                {
                    id = Controls.DownSelect(id, MiniMenu.Count);
                }
                if (key.Key == ConsoleKey.UpArrow)
                {
                    id = Controls.UpSelect(id, MiniMenu.Count);
                }
                if (key.Key == ConsoleKey.Enter)
                {
                    if (id == 1)
                    {
                        ObjectCreator.TimeLine(timeLines, people);
                        ConsoleHelper.WatchingResult();
                    }
                    if (id == 2)
                    {
                        ObjectCreator.Person(people);
                        ConsoleHelper.WatchingResult();
                    }
                    if (id == 3)
                    {
                        isMiniMenuActive = false;
                        ConsoleHelper.ClearScreen();
                    }
                }
            }
        }
    }
}
