﻿using System;
using System.Collections.Generic;
using System.Globalization;
using TimeLines.Core;

namespace TimeLines.ConsoleGUI
{
    class DataInput
    {
        public static DateTime ParseDate()
        {
            DateTime date = new DateTime();
            bool isCorrect = false;
            while (!isCorrect)
            {
                string value = Console.ReadLine();
                isCorrect = true;
                if (!DateTime.TryParseExact(value, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    if (!DateTime.TryParseExact(value, "yyyy-dd-MM", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                    {
                        if (!DateTime.TryParseExact(value, "yyyy-MM", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                        {
                            if (!DateTime.TryParseExact(value, "yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                            {
                                isCorrect = false;
                                Console.ForegroundColor = ConsoleColor.DarkRed;
                                Console.WriteLine("Неверный ввод даты");
                                Console.ForegroundColor = ConsoleColor.White;
                            }
                        }
                    }
                }
            }
            return date;
        }
        public static string EnterName()
        {
            Console.WriteLine("Введите ФИО человека");
            string name = Console.ReadLine();
            return name;
        }
        public static DateTime EnterBirthDate()
        {
            Console.WriteLine("Введите дату рождения");
            DateTime birthDate = ParseDate();
            return birthDate;
        }
        public static DateTime? EnterDeathDate()
        {
            Console.WriteLine("Нажмите кнопку F, чтобы ввести дату смерти, или любую другую кнопку если человек жив");
            DateTime? deathDate = null;
            ConsoleKeyInfo keyDate = Console.ReadKey();
            if (keyDate.Key == ConsoleKey.F)
            {
                Console.WriteLine("Введите дату смерти");
                deathDate = ParseDate();
            }
            return deathDate;
        }

        public static List<Person> EnterParents(List<Person> people)
        {
            List<Person> parents = new List<Person>();
            bool functionEnd = false;
            while (!functionEnd)
            {
                Console.WriteLine("Нажмите Enter, если у человека неизвестны родители или кнопку F, чтобы найти родственников");
                ConsoleKeyInfo keyParent = Console.ReadKey(true);
                if (keyParent.Key == ConsoleKey.F)
                    parents.Add(MainMenuItem.SearchPeople(people));
                else if (keyParent.Key == ConsoleKey.Enter)
                    functionEnd = true;
                else Console.WriteLine("Неверный ввод");
                if (parents.Count == 2) functionEnd = true;
            }
            if (parents.Count == 0) parents = null;
            return parents;
        }
        public static DateTime EnterYear()
        {
            Console.WriteLine("Введите год события");
            DateTime year = ParseDate();
            return year;
        }
        public static string EnterDescription()
        {
            Console.WriteLine("Введите описание события");
            string description = Console.ReadLine();
            return description;
        }
        public static List<Person> EnterParticipants(List<Person> people)
        {
            List<Person> participants = new List<Person>();
            bool functionEnd = false;
            while (!functionEnd)
            {
                Console.WriteLine("Добавляем участника события? (Y/N)");
                ConsoleKeyInfo key = Console.ReadKey(true);
                if (key.Key == ConsoleKey.Y)
                    participants.Add(MainMenuItem.SearchPeople(people));
                else if (key.Key == ConsoleKey.N) 
                    functionEnd = true;
            }
            return participants;
        }
    }
}
